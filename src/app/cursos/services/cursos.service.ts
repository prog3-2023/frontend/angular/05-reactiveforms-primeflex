import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Curso} from "../interfaces/curso.interface";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  private baseUrl: string = environment.baseUrl;

  constructor(private httpClient: HttpClient) {
  }

  getCursos(): Observable<Curso[]> {
    return this.httpClient.get<Curso[]>(this.baseUrl + '/cursos');
  }

  getCursoById(id: string): Observable<Curso> {
    return this.httpClient.get<Curso>(this.baseUrl + '/cursos/' + id);
  }
}
