import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-confirmar',
  templateUrl: './confirmar.component.html',
  styleUrls: ['./confirmar.component.scss']
})
export class ConfirmarComponent implements OnInit {

  constructor(private matDialogRef: MatDialogRef<ConfirmarComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { precio: number }) {
  }

  ngOnInit(): void {
  }

  cerrar() {
    this.matDialogRef.close();
  }

  confirmarIns() {
    this.matDialogRef.close(true);
  }
}
