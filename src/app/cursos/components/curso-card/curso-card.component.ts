import {Component, Input, OnInit} from '@angular/core';
import {Curso} from "../../interfaces/curso.interface";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmarComponent} from "../confirmar/confirmar.component";
import {InscripcionesService} from "../../../inscripciones/services/inscripciones.service";
import {Crearinscripcionrequestmodel} from "../../../inscripciones/interfaces/crearinscripcionrequestmodel.interface";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-curso-card',
  templateUrl: './curso-card.component.html',
  styleUrls: ['./curso-card.component.scss']
})
export class CursoCardComponent implements OnInit {

  @Input() curso!: Curso;

  constructor(private matDialog: MatDialog,
              private inscripcionesService: InscripcionesService,
              private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  inscribir() {
    const dialog = this.matDialog.open(ConfirmarComponent, {
      width: '500px',
      data: this.curso
    });

    dialog.afterClosed().subscribe(resp => {
      if (resp) {
        const inscripcion: Crearinscripcionrequestmodel = {id_curso: this.curso.id!, id_persona: 1};
        this.inscripcionesService.guardarInscripcion(inscripcion).subscribe(resp => {
          this.showSnackBar(`Se creo la inscripcion nro: ${resp}`)
        }, error => {
          this.showSnackBar(`Ya se encuentra inscripto al curso`)
        });
      }
    });
  }

  showSnackBar(mensaje: string) {
    this.matSnackBar.open(mensaje, 'Aceptar', {
      duration: 3000
    });
  }
}

